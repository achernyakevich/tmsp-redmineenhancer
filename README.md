## Intro ##

The `RedmineEnhancer.user.js` designed to bring additional functionality to
any [Redmine](https://redmine.org/)

## Contribution guidelines ##

If you would like to contribute - create a pull request.

If you need some features or would like to propose some features - create
an issue.

## Release Notes ##

## v. 0.3.0 and earlier ##

### Misc ###

* Migrated to new public repository
* Added metainformation for supporting automatic updates and public activities

## v. 0.2.2 and earlier ##

### Features ###

* Filtering issue relations table
* Filtering subtasks table
